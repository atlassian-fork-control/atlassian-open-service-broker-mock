package com.atlassian.openservicebroker.mock.service.creator;

import com.atlassian.openservicebroker.mock.helper.AsyncServiceUpdate;
import com.atlassian.openservicebroker.mock.model.ServiceInstance;
import com.atlassian.openservicebroker.mock.model.ServiceInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceRequest;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceResponse;
import org.springframework.stereotype.Component;

@Component("service-sync")
public class MockServiceCreatorSync implements MockServiceCreator {

    Logger logger = LoggerFactory.getLogger(MockServiceCreatorSync.class);

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Autowired
    AsyncServiceUpdate asyncServiceUpdate;

    @Override
    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest request) {
        String serviceInstanceId = request.getServiceInstanceId();
        logger.info("Creating service " + serviceInstanceId);
        ServiceInstance si = ServiceInstance.requestToBean(request);
        si.setLastOperation("created");
        serviceInstanceRepository.save(si);
        return new CreateServiceInstanceResponse()
                .withAsync(false)
                .withDashboardUrl(String.format("http://dashboard.url?instanceId=%s", si.getId()))
                .withInstanceExisted(false);
    }

}
